const path = require('path');
const webpackMiddleware = require("webpack-dev-middleware");
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
  template: './src/index.html',
  filename: 'index.html',
  inject: 'body'
});
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const devServer = require('webpack-dev-server');

module.exports = {

  entry: {app : './src/index.js'},
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'dist'),
    // publicPath: 'http://0.0.0.0:5000/'
  },
  plugins: [
    HtmlWebpackPluginConfig,
    new ExtractTextPlugin('main.css'),
    new webpack.LoaderOptionsPlugin({
            options: {
                postcss: [ // <---- postcss configs go here under LoadOptionsPlugin({ options: { ??? } })
                    require('postcss-cssnext'),
                    require('postcss-mixins'),
                    require('postcss-nested'),
                ]
            }
        }),
  ],

  module: {
      rules: [
        {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            use: [
              {
                loader: 'css-loader',
                options: {
                  importLoaders: 1
                },
              },
              'postcss-loader'
            ],
          }),
        },
        {
          test: /\.(png|svg|jpg|gif)$/,
          use: [
            'file-loader'
          ]
        },
        {
         test: /\.(woff|woff2|eot|ttf|otf)$/,
         use: [
           'file-loader'
         ]
       },
     ]
   },
   devServer: {
    contentBase: path.join(__dirname, "src"),
    compress: true,
    port: 5000
  },

};
