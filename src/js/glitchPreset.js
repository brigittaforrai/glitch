// import imageDataUrl from './toDataUrl.js';
import Animation from './glitchAnimation.js';
import Glitch from './glitch.js';
import Background from './backgroundGlitch.js'

export default class AnimationPreset {
  constructor(config) {
    this.parent = config.parentSelector || 'body';
    this.canvasWidth = config.canvasWidth || 500;
    this.imageUrl = config.imageUrl;
    this.loop = config.loop || 15;
    this.glitchNum = config.glitchNum || 3;
    this.overflow = config.overflow || false;
    this.time = config.time || 100;
    this.minGlitchWidth = config.minGlitchWidth || 1;
    this.minGlitchHeight = config.minGlitchHeight || 1;
    this.displaceX = config.displaceX || 3;
    this.displaceY = config.displaceY || 3;
    this.bgDisplaceX = config.bgDispalceX || 3;
    this.bgDisplaceY = config.bgDispalceY || 3;

    this.image = new Image();
    this.image.src = this.imageUrl;
    this.image.crossOrigin = 'anonymous';

    this.glitches = [];
    this.background = [];

    this.canvasHeight = null;
    this.parentNode = document.querySelector(this.parent);

    this.createReference();
    this.createCanvas();
    this.imageOnload();
  }

  createReference() {
    this.reference = document.createElement('canvas');
    this.parentNode.appendChild(this.reference)
    this.reference.id = 'glitch-reference';
    this.reference.width = this.canvasWidth;
    this.reference.height = null;

    this.rContext = this.reference.getContext('2d');
    this.rContext.fillStyle = 'rgba(0, 0, 0, 0)';
    this.rContext.fillRect(0, 0, this.canvasWidth, this.canvasHeight);
  }

  createCanvas() {
    this.canvas = document.createElement('canvas');
    this.parentNode.appendChild(this.canvas);
    this.canvas.id = 'glitch-canvas';
    this.canvas.width = this.canvasWidth;
    this.canvas.height = null;
  }

  imageOnload() {
    this.image.onload = () => {
      this.imgPositionX = 0;
      this.imgPositionY = 0;
      this.setSizes();

      this.rContext.drawImage(
        this.image, this.imgPositionX, this.imgPositionY, this.imgWidth, this.imgHeight
      );

      this.maxGlitchWidth = config.maxGlitchWidth || this.imgWidth/2;
      this.maxGlitchHeight = config.maxGlitchHeight || this.imgHeight/4;

      this.generateGlitches();
      this.background = new Background({
        canvasWidth : this.canvasWidth,
        canvasHeight : this.canvasHeight,
        rContext : this.rContext,
        loop: this.loop,
        displaceX: this.bgDisplaceX,
        displaceY: this.bgDisplaceY,
        imgHeight: this.imgHeight,
        imgWidth: this.imgWidth
      })
      this.animation = new Animation({
        glitches: this.glitches,
        canvasWidth: this.canvasWidth,
        canvasHeight: this.canvasHeight,
        rContext: this.rContext,
        canvas: this.canvas,
        loop: this.loop,
        glitchNum: this.glitchNum,
        imgWidth: this.imgWidth,
        imgHeight: this.imgHeight,
        time: this.time,
        displaceX: this.displaceX,
        displaceY: this.displaceY,
        background: this.background,
      });
    }
  }

  setSizes() {
    let ratio;
    if(this.overflow) {
      this.imgWidth = this.canvasWidth/2;
      ratio = this.image.width / this.imgWidth;
      this.imgHeight = this.image.height / ratio;
      this.canvasHeight = this.imgHeight*1.2;
      this.imgPositionX = (this.canvasWidth-this.imgWidth)/2;
      this.imgPositionY = (this.canvasHeight-this.imgHeight)/2;
    } else {
      this.imgWidth = this.canvasWidth;
      ratio = this.image.width / this.imgWidth;
      this.imgHeight = this.image.height / ratio;
      this.canvasHeight = this.imgHeight;
    }

    this.reference.height = this.canvasHeight;
    this.canvas.height = this.canvasHeight;
  }

  generateGlitches() {
    for(let num=0; num<(this.glitchNum * this.loop); num++) {
      let glitch = new Glitch(
        {
          canvasWidth: this.canvasWidth,
          canvasHeight: this.canvasHeight,
          imgWidth: this.imgWidth,
          imgHeight: this.imgHeight,
          rContext: this.rContext,
          canvas: this.canvas,
          minGlitchWidth: this.minGlitchWidth,
          maxGlitchWidth: this.maxGlitchWidth,
          minGlitchHeight: this.minGlitchHeight,
          maxGlitchHeight: this.maxGlitchHeight,
          displaceX: this.displaceX,
          displaceY: this.displaceY,
        }
      );
      this.glitches.push(glitch)
    }
  }
}


const config = {
  parentSelector: 'body',
  canvasWidth: 700,
  imageUrl: './../assets/logo1.svg',
  loop: 13,
  glitchNum: 7,
  minGlitchWidth: 50,
  maxGlitchWidth: 105,
  minGlitchHeight: 3,
  maxGlitchHeight: 15,
  overflow: true,
  time: 70,
  displaceX: 50,
  displaceY: 1,
  bgDisplaceX: 10,
  bgDisplaceY: 10
}

const glitchAnimaton = new AnimationPreset(config);
