import * as filters from './filters.js';
  // function randomDestionationX(w) {
  //   return randomInt(0, this.canvasWidth-w );
  // }
  //
  // function randomDestionationY(h) {
  //   return randomInt(0, this.canvasHeight-h);
  // }

  //destionation igy nem lesz jo
function randomDestionationX(x, w) {
  return randomInt(x, w-x );
}

function randomDestionationY(y,h) {
  return randomInt(y, h-y);
}

function randomBoolean() {
  return Math.round(Math.random());
}

function randomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function randomColor() {
  const red = randomInt(0, 255);
  const green = randomInt(0, 255);
  const blue = randomInt(0, 255);
  const t = 255;
  return {
    red: red,
    green: green,
    blue: blue,
    t: t
  }
}

function chance(percent, callback, data, firstPixel, devider) {
  const num = Math.ceil(100/percent);
  const randomChoice = randomInt(0, num)
  if(randomChoice == 0) {
    callback(data, firstPixel, devider);
  }
}

export {
  chance,
  randomDestionationX,
  randomDestionationY,
  randomBoolean,
  randomInt,
  randomColor
}
