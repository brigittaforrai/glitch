import * as randomiser from './randomiser.js';

function invert(data, firstPixel, devider) {
  for(let i=firstPixel; i<data.length/devider; i+=4) {
    data[i] = 255 - data[i];
    data[i+1] = 255 - data[i+1];
    data[i+2] = 255 - data[i+2];
  }
  return data;
}

function grain(data, firstPixel, devider) {
  for(let i=firstPixel; i<data.length/devider; i+=4) {
    data[i] = randomiser.randomInt(0, 255);
    data[i+1] = randomiser.randomInt(0, 255);
    data[i+2] = randomiser.randomInt(0, 255);
  }
  return data;
}

function transparency(data, firstPixel, devider) {
  const t = randomiser.randomInt(0, 255);
  for(let i=firstPixel; i<data.length/devider; i+=4) {
    data[i+3] = t;
  }
  return data;
}

function colorAlpha(data, firstPixel, devider) {
  const red = randomiser.randomInt(0, 255);
  const green = randomiser.randomInt(0, 255);
  const blue = randomiser.randomInt(0, 255);
  const transparency = randomiser.randomInt(0, 255);
  for(let i=firstPixel; i<data.length/devider; i+=4) {
    if(data[i+3] == 0) {
      data[i] = red;
      data[i+1] = green;
      data[i+2] = blue;
      data[i+3] = transparency;
    }
  }
  return data;
}

export {
  invert,
  grain,
  transparency,
  colorAlpha
}
