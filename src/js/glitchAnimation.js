import * as randomiser from './randomiser.js';

export default class Animation {
  constructor(config) {
    this.glitches = config.glitches;
    this.canvasWidth = config.canvasWidth;
    this.canvasHeight = config.canvasHeight;
    this.width = config.imgWidth;
    this.height = config.imgHeight;
    this.ref = config.rContext;
    this.canvas = config.canvas;
    this.loop = config.loop;
    this.glitchNum = config.glitchNum;
    this.time = config.time;
    this.displaceX = config.displaceX;
    this.displaceY = config.displaceY;
    this.background = config.background;

    this.counter = 0;
    this.bgCounter = 0;

    this.context = this.canvas.getContext('2d');
    window.requestAnimationFrame(this.draw.bind(this));
  }

  draw(timestamp) {
    this.context.putImageData(
      this.background[this.bgCounter].image,
      this.background[this.bgCounter].x,
      this.background[this.bgCounter].y
    );

    let start = this.counter;
    let end = this.counter + this.glitchNum;
    let newLoop = this.glitches.slice(start, end);

    for(let glitch=0; glitch<newLoop.length; glitch++) {
      this.context.putImageData(
        newLoop[glitch].data,
        newLoop[glitch].xPos,
        newLoop[glitch].yPos
      );
    }
    this.checkCounters();

    const time = (Math.random()* this.time) + 10;
    setTimeout(() => {
      window.requestAnimationFrame(this.draw.bind(this));
    }, this.time);
  }

  checkCounters() {
    if(this.counter + this.glitchNum > this.glitches.length) {
      this.counter = 0;
    } else {
      this.counter += this.glitchNum;
    }
    if(this.bgCounter + 1 >= this.background.length) {
      this.bgCounter = 0;
    } else {
      this.bgCounter += 1;
    }
  }
}
