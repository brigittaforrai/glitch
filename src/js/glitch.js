import * as randomiser from './randomiser.js';
import * as filters from './filters.js';

export default class Glitch {
  constructor(config) {
    this.minGlitchWidth = config.minGlitchWidth;
    this.maxGlitchWidth = config.maxGlitchWidth;
    this.minGlitchHeight = config.minGlitchHeight;
    this.maxGlitchHeight = config.maxGlitchHeight;
    this.canvasWidth = config.canvasWidth;
    this.canvasHeight = config.canvasHeight;
    this.ref = config.rContext;
    this.canvas = config.canvas;
    this.width = config.imgWidth;
    this.height = config.imgHeight;
    this.displaceX = config. displaceX;
    this.displaceY = config.displaceY;
    this.createGlitch();

    return this.glitch;
  }

  createGlitch() {
    const w = randomiser.randomInt(this.minGlitchWidth, this.maxGlitchWidth);
    const h = randomiser.randomInt(this.minGlitchHeight, this.maxGlitchHeight);

    const x = randomiser.randomInt((this.canvasWidth-this.width)/2, (this.canvasWidth-this.width)/2+this.width-w);
    const y = randomiser.randomInt((this.canvasHeight-this.height)/2, (this.canvasHeight-this.height)/2+this.height-h);

    this.glitch = {
      data: this.ref.getImageData(x, y, w, h),
      xPos: this.randomDestionationX(x),
      yPos: this.randomDestionationY(y)
    }
    this.glitchData = this.glitch.data.data;

    Object.keys(filters).forEach(filterName => {
      var filter = filters[filterName];
      randomiser.chance(33, filter, this.glitchData, 0, 1);
    });

    // randomiser.chance(50, filters.transparency, this.glitchData, 0, 1);
    // randomiser.chance(30, filters.invert, this.glitchData, 0, 1);
    // randomiser.chance(30,filters.colorAlpha, this.glitchData,0, 1);
    // randomiser.chance(50,filters.grain, this.glitchData, 0, 1);
  }

  randomDestionationX(x) {
    // return this.randomInt(0, this.canvasWidth-w );
    // return 2*x * Math.sin(2*x);
    return randomiser.randomInt(x-this.displaceX*10, x+this.displaceX*10);
  }
  randomDestionationY(y) {
    return randomiser.randomInt(y-this.displaceY*10, y+this.displaceY*10);
    // return 2*y * Math.sin(2*y);
    // return this.randomInt(0, this.canvasHeight-h);
  }
}
