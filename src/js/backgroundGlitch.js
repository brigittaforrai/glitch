import * as randomiser from './randomiser.js';

export default class Background {
  constructor(config) {
    this.canvasWidth = config.canvasWidth;
    this.canvasHeight = config.canvasHeight;
    this.height = config.imgHeight;
    this.width = config.imgWidth;
    this.ref = config.rContext;
    this.loop = config.loop;
    this.displaceX = config.displaceX;
    this.displaceY = config.displaceY;
    this.background = [];

    this.generateBackground();

    return this.background;
  }

  generateBackground() {
    for(let i=0; i<this.loop; i++) {
      const choice = randomiser.randomBoolean();

      if(choice === 0) {
          const image = this.ref.getImageData(0, 0, this.canvasWidth, this.canvasHeight);
          this.background.push({
            image: image,
            x: 0,
            y: 0
          });
      }

      if(choice === 1) {
        const x = randomiser.randomInt((this.canvasWidth-this.width)/2, (this.canvasWidth-this.width)/2+this.width);
        const y = randomiser.randomInt((this.canvasHeight-this.height)/2, (this.canvasHeight-this.height)/2+this.height);

        const image = this.ref.getImageData(x, y, this.canvasWidth * Math.random()+1, this.canvasHeight * Math.random()+1);
        this.background.push({
          image: image,
          x: x + Math.sin(x)* this.displaceX,
          y: y+ Math.sin(y)*this.displaceY
        })
      }
      // if(choice === 2) {
      //
      // }
    }
  }

  // drawBackground() {
  //   if(randomiser.randomBoolean() == 0) {
  //     this.image = this.ref.getImageData(0, 0, this.canvasWidth, this.canvasHeight);
  //     this.context.putImageData(this.image, randomiser.randomBoolean(), randomiser.randomBoolean());
  //   }
  //
  //   let type = this.randomInt(0, 3);
  //
  //   if(type === 1) {
  //     const x = randomiser.randomInt((this.canvasWidth-this.width)/2, (this.canvasWidth-this.width)/2+this.width);
  //     const y = randomiser.randomInt((this.canvasHeight-this.height)/2, (this.canvasHeight-this.height)/2+this.height);
  //     this.image = this.ref.getImageData(x, y, this.canvasWidth * Math.random()+1, this.canvasHeight * Math.random()+1);
  //     this.context.putImageData(this.image, x + Math.sin(x)* this.displaceX, y+ Math.sin(y)*this.displaceY);
  //   }
  // }
}
